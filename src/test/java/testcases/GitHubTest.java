package testcases;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import services.Github;
/**
 * create  service testcases
 *
 */
public class GitHubTest {
    Response response;

    @Test
    public  void test01_createRepo(){
        Github github = new Github();
        response = github.createRepository("My_Api_Repo","this is api repo");
        Assert.assertEquals(response.getStatusCode(),201);
        Assert.assertTrue(response.getBody().asString().contains("name"));
    }
    @Test
    public  void test02_getRepos(){
        Github github = new Github();
        response = github.getUserRepositories();
        Assert.assertEquals(response.getStatusCode(),200);
    }

    @Test
    public  void test03_deleteRepo(){
        Github github = new Github();
        response = github.deleteRepository("My_Api_Repo");
        Assert.assertEquals(response.getStatusCode(),204);
    }

}
