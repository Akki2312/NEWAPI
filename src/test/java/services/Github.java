package services;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import testutilities.TestUtilities;


public class Github {

    Response response;

    public  Response createRepository(String reponame,String description)
    {
        RestAssured.baseURI = TestUtilities.getProperty("baseurl");
        RequestSpecification request = RestAssured.given().header("Authorization","token "+TestUtilities.getProperty("tokenvalue")).header("Content-Type","application/json");
        JSONObject requestParams = new JSONObject();
        requestParams.put("name",reponame);
        requestParams.put("description",description);
        requestParams.put("auto_init", true);
        requestParams.put("private", false);
        requestParams.put("gitignore_template", "nanoc");
        request.body(requestParams.toJSONString());
        response = request.post("/user/repos");
        return  response;
    }
    public  Response getUserRepositories()
    {
        RestAssured.baseURI = TestUtilities.getProperty("baseurl");
        RequestSpecification request = RestAssured.given().header("Authorization","token "+TestUtilities.getProperty("tokenvalue")).header("Content-Type","application/json");
        response = request.get("/user/repos");
        return  response;
    }
    public  Response deleteRepository(String reponame)
    {
        RestAssured.baseURI = TestUtilities.getProperty("baseurl");
        RequestSpecification request = RestAssured.given().header("Authorization","token "+TestUtilities.getProperty("tokenvalue")).header("Content-Type","application/json");
        String name= TestUtilities.getProperty("username");
        response = request.delete("/repos/"+name+"/"+reponame);
        return  response;
    }

}
