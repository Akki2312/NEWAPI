package testutilities;

import java.io.FileInputStream;
import java.util.Properties;

public class TestUtilities {
    private static Properties defaultProps = new Properties();
    static {
        try {
        String currentDir = System.getProperty("user.dir");
        String filepath=currentDir+"/src/test/java/config/config.properties";
        FileInputStream in = new FileInputStream(filepath);
        defaultProps.load(in);
        in.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
}

    /**
     * gets key value from properties file
     * @param key
     * @return values for specific key
     */
    public static String getProperty(String key) {
        return defaultProps.getProperty(key);
    }



}
